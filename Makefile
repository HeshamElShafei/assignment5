CC = g++

all: hello

doc: arrayListType.h main_prog.cc polynomialType.h polynomialTypeImp.cc
	doxygen -g
	doxygen Doxyfile

hello: main_prog.cc arrayListType.h polynomialType.h polynomialTypeImp.cc
	$(CC) $(CFLAGS) main_prog.cc polynomialTypeImp.cc
	
clean:
	rm -rf *o hello html latex a.out Doxyfile
	
	
