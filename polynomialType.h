#ifndef H_polynomial
#define	H_polynomial
 
#include <iostream>
#include "arrayListType.h"

using namespace std;

class polynomialType: public arrayListType<double>
{
	/** @breif overload stream insertion operator
	     */
	friend ostream& operator<<(ostream&, const polynomialType&);
	/** @breif overload stream extraction operator	
	*/
	friend istream& operator>>(istream&, polynomialType&);
		
	/** @breif overload the operator +
	*/
	friend polynomialType operator+(const polynomialType&, const polynomialType&);
	/** @breif overload the operator -
	*/
	friend polynomialType operator-(const polynomialType&, const polynomialType&);
		

		
public:
	/** @breif constructor
	*/
	polynomialType(int size = 100);
		
	/** @breif overload the operator () to evaluate the
	 *  polynomial at a given point
	 *  @return The value of the plynomial at x
	 *		         is calculated and returned
	*/	
	double operator() (double x);
		
	/** @return Function to return the smaller of x and y
	*   @param x is an integer to be compared with y
	*	@param y is an integer to be compared with x
	*/
	int min(int x, int y) const;
		/** @return Function to return the larger of x and y
		*    @param x is an integer to be compared with y
		*   @param y is an integer to be compared with x
		*/
	int max(int x, int y) const;
		
};

#endif